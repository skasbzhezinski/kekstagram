// Функция, возвращающая случайное целое число из переданного диапазона включительно.
const getRandomInRange = (min, max) => {
  if (min < 0 || max < 0) {
    return -1;
  }
  if (max < min) {
    [min, max] = [max, min];
  }
  return Math.floor(Math.random() * (max - min +1)) + min;
};

// console.log(getRandomInRange(1, 10));
getRandomInRange(1, 10);

// Функция для проверки максимальной длины строки.
const stringCount = (text, maxLength) => {
  return text.length  <= maxLength;
};

// console.log(stringCount('Это проверочное сообщение', 140));
stringCount('Это проверочное сообщение', 140);
